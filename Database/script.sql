create database Ecommerce
USE Ecommerce
GO
/****** Object:  Database [Ecommerce]    Script Date: 8/14/2020 11:44:55 AM ******/
CREATE DATABASE [Ecommerce]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Ecommerce', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\Ecommerce.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Ecommerce_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\Ecommerce_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Ecommerce] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Ecommerce].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Ecommerce] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Ecommerce] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Ecommerce] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Ecommerce] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Ecommerce] SET ARITHABORT OFF 
GO
ALTER DATABASE [Ecommerce] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Ecommerce] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Ecommerce] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Ecommerce] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Ecommerce] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Ecommerce] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Ecommerce] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Ecommerce] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Ecommerce] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Ecommerce] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Ecommerce] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Ecommerce] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Ecommerce] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Ecommerce] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Ecommerce] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Ecommerce] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Ecommerce] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Ecommerce] SET RECOVERY FULL 
GO
ALTER DATABASE [Ecommerce] SET  MULTI_USER 
GO
ALTER DATABASE [Ecommerce] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Ecommerce] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Ecommerce] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Ecommerce] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Ecommerce] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Ecommerce', N'ON'
GO
ALTER DATABASE [Ecommerce] SET QUERY_STORE = OFF
GO
USE [Ecommerce]
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [Ecommerce]
GO
/****** Object:  Table [dbo].[Cart]    Script Date: 8/14/2020 11:45:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cart](
	[cartId] [int] IDENTITY(1,1) NOT NULL,
	[productId] [int] NOT NULL,
	[quantity] [int] NOT NULL,
	[userId] [int] NOT NULL,
	[productName] [nvarchar](max) NULL,
	[price] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Cart] PRIMARY KEY CLUSTERED 
(
	[cartId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Category]    Script Date: 8/14/2020 11:45:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[categoryId] [int] IDENTITY(1,1) NOT NULL,
	[categoryName] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Category] PRIMARY KEY CLUSTERED 
(
	[categoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Order]    Script Date: 8/14/2020 11:45:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[oid] [int] IDENTITY(1,1) NOT NULL,
	[orderDate] [datetime] NOT NULL,
	[totalPrice] [int] NOT NULL,
	[deliverCharges] [int] NOT NULL,
	[userId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Order] PRIMARY KEY CLUSTERED 
(
	[oid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 8/14/2020 11:45:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[productId] [int] IDENTITY(1,1) NOT NULL,
	[productName] [nvarchar](max) NULL,
	[price] [int] NOT NULL,
	[quantity] [int] NOT NULL,
	[description] [nvarchar](max) NULL,
	[categoryId] [int] NOT NULL,
	[cartId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Product] PRIMARY KEY CLUSTERED 
(
	[productId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductOrder]    Script Date: 8/14/2020 11:45:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductOrder](
	[prodOrdId] [int] IDENTITY(1,1) NOT NULL,
	[orderId] [int] NOT NULL,
	[productId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.ProductOrder] PRIMARY KEY CLUSTERED 
(
	[prodOrdId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 8/14/2020 11:45:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[userId] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NULL,
	[address] [nvarchar](max) NULL,
	[phoneNo] [nvarchar](max) NULL,
	[email] [nvarchar](max) NULL,
	[password] [nvarchar](max) NULL,
	[gender] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.User] PRIMARY KEY CLUSTERED 
(
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Cart] ON 

INSERT [dbo].[Cart] ([cartId], [productId], [quantity], [userId], [productName], [price]) VALUES (4, 10, 2, 5, N'Washing Machine', 20000)
INSERT [dbo].[Cart] ([cartId], [productId], [quantity], [userId], [productName], [price]) VALUES (5, 6, 5, 5, N'Harry Potter', 2500)
INSERT [dbo].[Cart] ([cartId], [productId], [quantity], [userId], [productName], [price]) VALUES (6, 22, 2, 6, N'Redmi Note 8', 3000)
INSERT [dbo].[Cart] ([cartId], [productId], [quantity], [userId], [productName], [price]) VALUES (9, 24, 10, 5, N'Trimmer', 4000)
SET IDENTITY_INSERT [dbo].[Cart] OFF
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([categoryId], [categoryName]) VALUES (1, N'Electronics')
INSERT [dbo].[Category] ([categoryId], [categoryName]) VALUES (2, N'Books')
INSERT [dbo].[Category] ([categoryId], [categoryName]) VALUES (4, N'Family Household')
SET IDENTITY_INSERT [dbo].[Category] OFF
SET IDENTITY_INSERT [dbo].[Order] ON 

INSERT [dbo].[Order] ([oid], [orderDate], [totalPrice], [deliverCharges], [userId]) VALUES (8, CAST(N'2020-01-20T00:00:00.000' AS DateTime), 500, 0, 5)
INSERT [dbo].[Order] ([oid], [orderDate], [totalPrice], [deliverCharges], [userId]) VALUES (9, CAST(N'2019-12-12T00:00:00.000' AS DateTime), 200, 50, 6)
INSERT [dbo].[Order] ([oid], [orderDate], [totalPrice], [deliverCharges], [userId]) VALUES (11, CAST(N'2020-01-15T00:00:00.000' AS DateTime), 480, 50, 5)
INSERT [dbo].[Order] ([oid], [orderDate], [totalPrice], [deliverCharges], [userId]) VALUES (15, CAST(N'2019-12-12T00:00:00.000' AS DateTime), 200, 50, 6)
SET IDENTITY_INSERT [dbo].[Order] OFF
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([productId], [productName], [price], [quantity], [description], [categoryId], [cartId]) VALUES (6, N'Harry Potter', 500, 10, N'Philosophy', 2, 4)
INSERT [dbo].[Product] ([productId], [productName], [price], [quantity], [description], [categoryId], [cartId]) VALUES (22, N'Redmi Note 8 PRO', 15000, 10, N'Smartphone', 1, 4)
INSERT [dbo].[Product] ([productId], [productName], [price], [quantity], [description], [categoryId], [cartId]) VALUES (24, N'Trimmer', 800, 20, N'Hair trimming', 1, 4)
INSERT [dbo].[Product] ([productId], [productName], [price], [quantity], [description], [categoryId], [cartId]) VALUES (25, N'Galaxy s8', 20000, 20, N'Smartphone', 1, 5)
INSERT [dbo].[Product] ([productId], [productName], [price], [quantity], [description], [categoryId], [cartId]) VALUES (27, N'Cleaning Mob', 700, 10, N'Cleaning', 4, 6)
INSERT [dbo].[Product] ([productId], [productName], [price], [quantity], [description], [categoryId], [cartId]) VALUES (28, N'Lyzol', 100, 20, N'Cleaning Liquid', 4, 5)
INSERT [dbo].[Product] ([productId], [productName], [price], [quantity], [description], [categoryId], [cartId]) VALUES (1031, N'Redmi Note 8 pr0', 15000, 10, N'Smartphone', 1, 4)
INSERT [dbo].[Product] ([productId], [productName], [price], [quantity], [description], [categoryId], [cartId]) VALUES (1032, N'Samsung M3', 12000, 5, N'Smartphone', 1, 4)
INSERT [dbo].[Product] ([productId], [productName], [price], [quantity], [description], [categoryId], [cartId]) VALUES (1036, N'One Plus 7T', 37000, 10, N'Smartphone', 1, 4)
SET IDENTITY_INSERT [dbo].[Product] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([userId], [name], [address], [phoneNo], [email], [password], [gender]) VALUES (5, N'Rohit', N'Mumbai', N'9878521214', N'rohitman@gmail.com', N'', N'Male')
INSERT [dbo].[User] ([userId], [name], [address], [phoneNo], [email], [password], [gender]) VALUES (6, N'Aniket', N'Pune', N'7038295412', N'aniketmad@gmail.com', N'', N'Male')
INSERT [dbo].[User] ([userId], [name], [address], [phoneNo], [email], [password], [gender]) VALUES (1008, N'Vishal', N'Latur', N'9856966', N'vish@gmail.com', N'', N'Male')
INSERT [dbo].[User] ([userId], [name], [address], [phoneNo], [email], [password], [gender]) VALUES (1010, N'Aniket', N'Pune', N'7038295412', N'rohitman@gmail.com', N'', N'Male')
SET IDENTITY_INSERT [dbo].[User] OFF
ALTER TABLE [dbo].[Cart]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Cart_dbo.User_userId] FOREIGN KEY([userId])
REFERENCES [dbo].[User] ([userId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Cart] CHECK CONSTRAINT [FK_dbo.Cart_dbo.User_userId]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Users_userId] FOREIGN KEY([userId])
REFERENCES [dbo].[User] ([userId])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Users_userId]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Product_dbo.Cart_cartId] FOREIGN KEY([cartId])
REFERENCES [dbo].[Cart] ([cartId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_dbo.Product_dbo.Cart_cartId]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Product_dbo.Category_categoryId] FOREIGN KEY([categoryId])
REFERENCES [dbo].[Category] ([categoryId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_dbo.Product_dbo.Category_categoryId]
GO
ALTER TABLE [dbo].[ProductOrder]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProductOrder_dbo.Order_orderId] FOREIGN KEY([orderId])
REFERENCES [dbo].[Order] ([oid])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProductOrder] CHECK CONSTRAINT [FK_dbo.ProductOrder_dbo.Order_orderId]
GO
ALTER TABLE [dbo].[ProductOrder]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProductOrder_dbo.Product_productId] FOREIGN KEY([productId])
REFERENCES [dbo].[Product] ([productId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProductOrder] CHECK CONSTRAINT [FK_dbo.ProductOrder_dbo.Product_productId]
GO
USE [master]
GO
ALTER DATABASE [Ecommerce] SET  READ_WRITE 
GO
