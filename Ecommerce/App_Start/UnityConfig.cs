using Ecommerce.Repository;
using System.Web.Http;
using Unity;
using Unity.WebApi;

namespace Ecommerce
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            container.RegisterType<IProductRepository, ProductRepository>();
            container.RegisterType<ICategoryRepository, CategoryRepository>();
            container.RegisterType<IOrderRepository, OrderRepository>();
            container.RegisterType<IUserRepository, UserRepository>();
            container.RegisterType<ICartRepository, CartRepository>();
          
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}