﻿using Ecommerce.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataAceessLayer;
using System.Web.Http.Description;
using System.Data.Entity.Infrastructure;
using Ecommerce.Models;
using Ecommerce.Filters;

namespace Ecommerce.Controllers
{
    [RoutePrefix("Product")]
    [ExceptionFilter]
    //[Authorize]
    [ActionFilter]
    //[NotImplExceptionFilter]
    /*  We can do Authorization depending on Role and Username
      [Authorize(Users = "")]  // Restrict by user:
    [Authorize(Roles = "")] ///Restrict by Role:*/
    public class ProductsController : ApiController
    {
        
        private IProductRepository productRepository;
        //Constructor Injection
        public ProductsController(IProductRepository _productRepository)
        {
            this.productRepository = _productRepository;
        }
        /*public ProductsController()
        {
            this.productRepository = new ProductRepository(new EcommerceEntities());
        }*/
        [Authorize]
        [HttpGet]
        [Route("GetProducts")]
        public IEnumerable<Object> GetProducts()
        {
            return productRepository.GetProducts();

        }

        [HttpGet]
        [Route("GetProductById/{id}")]
        public Object GetProductById([FromUri] int id)
        {
            
            return productRepository.GetProductById(id);
        }

        // PUT: api/Products/5
        [ResponseType(typeof(void))]
        [Route("PutProduct/{id}")]
        public IHttpActionResult PutProduct(int id, ProductDto product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != product.productId)
            {
                return BadRequest();
            }

            try
            {
                productRepository.PutProduct(id, product);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!productRepository.ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpPost]
        [Route("PostProduct")]
        public HttpResponseMessage PostProduct([FromBody]ProductDto obj)
        {
            try
            {
                {
                    productRepository.PostProduct(obj);
                }
                var msg = Request.CreateResponse(HttpStatusCode.Created, obj);
                msg.Headers.Location = new Uri(Request.RequestUri
                    + obj.productId.ToString());
                return msg;
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);
            }
        }

        //post through url
        [HttpPost]
        [Route("PostData/{productName}/{price}/{quantity}/{description}/{categoryId}/{cartId}")]
        public HttpResponseMessage PostData(string productName, int price, int quantity, string description, int categoryId, int cartId)
        {
            Product obj = new Product
            {
                productName = productName,
                price = price,
                quantity = quantity,
                description = description,
                categoryId = categoryId,
                cartId = cartId
            };
            try
            {  
                    productRepository.PostData(obj);
                
                var msg = Request.CreateResponse(HttpStatusCode.Created, obj);
                msg.Headers.Location = new Uri(Request.RequestUri
                    + obj.productId.ToString());
                return msg;
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);
            }

        }

        [HttpDelete]
        [Route("DeleteProduct/{id}")]
        public HttpResponseMessage DeleteProduct(int id)
        {
            bool status = productRepository.DeleteProduct(id);
            if (status)
            {
                var msg = Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                return msg;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    "Requested Data Not found");
            }
        }

        public bool ProductExists(int id)
        {
            return productRepository.ProductExists(id);
        }

        [HttpGet]
        [Route("GetSomething")]
        public int GetSomething()
        {
            int a = 1;
            return a / 0;

        }

        [HttpGet]
        [Route("GetData")]
        public void GetData()
        {
            throw new NotImplementedException();
        }
        
    }
}
