﻿using DataAceessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Ecommerce.Controllers
{
    [RoutePrefix("Login")]
    public class LoginController : ApiController
    {
        EcommerceEntities db = new EcommerceEntities();



        [Route("UserLogin")]
        [HttpPost]
        public HttpResponseMessage UserLogin([FromBody]User user)
        {
            User c = db.Users.Where(x => x.name == user.name).FirstOrDefault();

            if (c == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, "The user was not found.");

            bool credentials = db.Users.Any(x => x.name == user.name && x.password == user.password);

            if (!credentials) return Request.CreateResponse(HttpStatusCode.Forbidden,
                "The username/password combination was wrong.");

            return Request.CreateResponse(HttpStatusCode.OK, TokenManager.GenerateToken(c.name));

        }

        //[Route("Validate")]
        //[HttpGet]
        //public HttpResponseMessage Validate(string token, string username)
        //{
        //    int UserId = new UserRepository().GetUser(username);
        //    if (UserId == 0) return new ResponseVM
        //    {
        //        Status = "Invalid",
        //        Message = "Invalid User."
        //    };
        //    string tokenUsername = TokenManager.ValidateToken(token);
        //    if (username.Equals(tokenUsername))
        //    {
        //        return new ResponseVM
        //        {
        //            Status = "Success",
        //            Message = "OK",
        //        };
        //    }
        //    return new ResponseVM
        //    {
        //        Status = "Invalid",
        //        Message = "Invalid Token."
        //    };
        //}
    }
}
