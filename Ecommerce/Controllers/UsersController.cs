﻿using Ecommerce.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataAceessLayer;
using System.Web.Http.Description;
using System.Data.Entity.Infrastructure;
using Ecommerce.Models;

namespace Ecommerce.Controllers
{
    [RoutePrefix("User")]
    public class UsersController : ApiController
    {
        private IUserRepository userRepository;
      
        /*public UsersController()
        {
            this.userRepository = new UserRepository(new EcommerceEntities());
        }*/

        public UsersController(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        [HttpGet]
        [Route("GetUsers")]
        public IEnumerable<Object> GetUsers()
        {

            return userRepository.GetUsers();
        }

        [HttpGet]
        [Route("GetUserById/{id}")]
        public Object GetUserById([FromUri] int id)
        {
            return userRepository.GetUserById(id);
        }


        [HttpPost]
        [Route("PostUser")]
        public HttpResponseMessage PostUser([FromBody]UserDto obj)
        {
            try
            {
                userRepository.PostUser(obj);
                var msg = Request.CreateResponse(HttpStatusCode.Created, obj);
                msg.Headers.Location = new Uri(Request.RequestUri
                    + obj.userId.ToString());
                return msg;
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);
            }
        }

        // PUT: api/Users/5
        [ResponseType(typeof(void))]
        [Route("PutUser/{id}")]
        public IHttpActionResult PutUser(int id, UserDto user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.userId)
            {
                return BadRequest();
            }
            
            try
            {
                userRepository.PutUser(id, user);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        [HttpDelete]
        [Route("DeleteUser/{id}")]
        public HttpResponseMessage DeleteUser(int id)
        {
            bool status = userRepository.DeleteUser(id);
            if (status)
            {
                var msg = Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                return msg;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    "Requested Data Not found");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                userRepository.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(int id)
        {
            return userRepository.UserExists(id);
        }
    }
}
