﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAceessLayer;
using System.Data.Entity;
using Ecommerce.Models;
using AutoMapper;

namespace Ecommerce.Repository
{
    public class UserRepository:IUserRepository,IDisposable
    {
        private EcommerceEntities db;
        public UserRepository(EcommerceEntities db)
        {
            this.db = db;
        }
        public IList<UserDto> GetUsers()
        {
            var query = db.Users;
            return query.ToList().Select(cat => Mapper.Map<User, UserDto>(cat)).ToList();
        }
        public UserDto GetUserById(int id)
        {
            var query = db.Users.Where(u => u.userId == id).FirstOrDefault();
            return Mapper.Map<User, UserDto>(query);
        }
        public void PutUser(int id, UserDto obj1)
        {
            User user = Mapper.Map<UserDto, User>(obj1);
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();
        }


        public void PostUser(UserDto obj1)
        {
            User obj = Mapper.Map<UserDto, User>(obj1);
            db.Users.Add(obj);
            db.SaveChanges();
        }
        public bool DeleteUser(int id)
        {
            bool status = false;
            User user = db.Users.Where(u => u.userId == id).FirstOrDefault();
            if (user != null)
            {
                status = true;
                db.Users.Remove(user);
                db.SaveChanges();
                return status;
            }
            else
            {
                return status;
            }
        }

        public bool UserExists(int id)
        {
            return db.Users.Count(e => e.userId == id) > 0;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}