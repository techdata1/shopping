﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAceessLayer;
using System.Data.Entity;
using Ecommerce.Models;
using AutoMapper;

namespace Ecommerce.Repository
{
    public class OrderRepository:IOrderRepository,IDisposable
    {
        private EcommerceEntities db;
        public OrderRepository(EcommerceEntities db)
        {
            this.db = db;
        }

         //maping
        public IList<OrderDto> GetOrders()
        {
            var query = db.Orders;
            return query.ToList().Select(cat => Mapper.Map<Order, OrderDto>(cat)).ToList();
        }

        /*public IQueryable<OrderDto> GetOrders()
        {
            var query = from o in db.Orders
                        select new OrderDto
                        {
                            oid=o.oid,
                            orderDate=o.orderDate,
                            deliverCharges=o.deliverCharges,
                            totalPrice=o.totalPrice,
                            userId=o.userId
                        }
                        ;
            return query;
        }*/

         public IList<OrderDto> GetOrderByUid(int uid)
         {
             var query = (from o in db.Orders
                          where o.userId == uid
                          select o);
             return query.ToList().Select(cat => Mapper.Map<Order, OrderDto>(cat)).ToList();

         }

        /*public IQueryable<OrderDto> GetOrderByUid(int uid)
        {
            var query = (from o in db.Orders
                         where o.userId == uid
                         select new OrderDto
                         {
                             oid = o.oid,
                             orderDate = o.orderDate,
                             deliverCharges = o.deliverCharges,
                             totalPrice = o.totalPrice,
                             userId = o.userId
                         });
            return query;

        }*/

         public IList<OrderDto> GetRecentOrders()
         {
             var testdate = DateTime.Now.AddMonths(-6);
             var query = (from o in db.Orders
                          join u in db.Users
                          on o.userId equals u.userId
                          where o.orderDate <= testdate
                          select o);
             return query.ToList().Select(cat => Mapper.Map<Order, OrderDto>(cat)).ToList();

         }
        /*public IQueryable<Order> GetRecentOrders()
        {
            var testdate = DateTime.Now.AddMonths(-6);
            var query = (from o in db.Orders
                         join u in db.Users
                         on o.userId equals u.userId
                         where o.orderDate <= testdate
                         select o);
            return query;

        }*/
        public void PutOrder(int id, OrderDto obj)
        {
            Order order = Mapper.Map<OrderDto, Order>(obj);
            db.Entry(order).State = EntityState.Modified;
            db.SaveChanges();
        }


        public void PostOrder(OrderDto obj)
        {
            Order order = Mapper.Map<OrderDto, Order>(obj);
            db.Orders.Add(order);
            db.SaveChanges();
        }
        public bool DeleteOrder(int id)
        {
            bool status = false;
            Order order = db.Orders.Where(o => o.oid == id).FirstOrDefault();
            if (order != null)
            {
                status = true;
                db.Orders.Remove(order);
                db.SaveChanges();
                return status;
            }
            else
            {
                return status;
            }
        }

        public bool OrderExists(int id)
        {
            return db.Users.Count(e => e.userId == id) > 0;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}