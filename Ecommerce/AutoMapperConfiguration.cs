﻿using AutoMapper;
using DataAceessLayer;
using Ecommerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce
{
    public class AutoMapperConfiguration:Profile
    {
        public static void configure()
        {
            Mapper.Initialize(cfg=> {
              
                cfg.CreateMap<Category, CategoryDto>();
                cfg.CreateMap<User, UserDto>();
                cfg.CreateMap<Product, ProductDto>();
                cfg.CreateMap<Order, OrderDto>();
                cfg.CreateMap<Cart, CartDto>();

                cfg.CreateMap<CategoryDto, Category>()
                .ForMember(d => d.Products, opt => opt.Ignore());

                cfg.CreateMap<UserDto, User>()
                .ForMember(d => d.Orders, opt => opt.Ignore())
                .ForMember(d => d.Carts, opt => opt.Ignore());

                cfg.CreateMap<ProductDto, Product>()
                .ForMember(d => d.Cart, opt => opt.Ignore())
                .ForMember(d => d.Category, opt => opt.Ignore())
                .ForMember(d => d.ProductOrders, opt => opt.Ignore());

                cfg.CreateMap<OrderDto, Order>()
                 .ForMember(d => d.ProductOrders, opt => opt.Ignore())
                .ForMember(d => d.User, opt => opt.Ignore());

                cfg.CreateMap<CartDto, Cart>()
                .ForMember(d => d.User, opt => opt.Ignore())
                .ForMember(d => d.Products, opt => opt.Ignore());


            });

            Mapper.AssertConfigurationIsValid();
        }

        public static void run()
        {
            configure();
            Mapper.AddProfile<AutoMapperConfiguration>();
        }
    }
}